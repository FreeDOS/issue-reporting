# Issue Reporting

This project is for users to report bugs with or without a GitLab account.

It is also good for submitting an issue that does not really fit in a specific project.
